class qt::params {
  # ONLINE INSTALLER 
  # Linux x64: "http://download.qt.io/official_releases/online_installers/qt-unified-linux-x64-online.run"
  # Linux x86: "http://download.qt.io/official_releases/online_installers/qt-unified-linux-x86-online.run"
  # Mac x64:   "http://download.qt.io/official_releases/online_installers/qt-unified-mac-x64-online.dmg"
  # Win x86:   "http://download.qt.io/official_releases/online_installers/qt-unified-windows-x86-online.exe"

  if 'amd64' == $architecture {
    $installer_arch = 'x64'
  }
  else {
    $installer_arch = 'x86'
  }

  if ('Ubuntu' == $operatingsystem) or ('Debian' == $operatingsystem) { 
    $installer_name = "qt-unified-linux-${installer_arch}-online.run"
  }
  elsif 'Darwin' == $operatingsystem {
    $installer_name = "qt-unified-mac-x64-online.dmg"
  }
  elsif 'windows' == $operatingsystem {
    $installer_name = "qt-unified-windows-x86-online.exe"
  }
  else {
    notice ("$operatingsystem not supported by qt::installer")
  }

  # ONLINE INSTALLERS
  if ('windows' == $operatingsystem) { 
    $installer_defaults = {
      root => "c:/tmp/",
      name => "${installer_name}",
      destination => "c:/qt",
    }
  }
  else {
    $installer_defaults = {
      root => "/tmp/",
      name => "${installer_name}",
      destination => "/opt/qt",
    }
  }

  $downloader_defaults = {
    root => $installer_defaults['root'],
    name => "${installer_name}",
    url => "http://download.qt.io/official_releases/online_installers/${installer_name}",
    verbose => true,
    timeout => 300,
  }

}
