class qt ($root = $::qt::installer_defaults['destination'],
          $components = {} ) inherits ::qt::params {  
  
  ##################################################
  # GET INSTALLER & EXECUTE
  ##################################################

  # qt::downloader will download the online installer and put it to /tmp
  class { 'qt::downloader' : }

  # qt::installer starts the default installation of qt 
  class { 'qt::installer' : 
    install_settings => { destination => "$root", 
                          version => "$version", },
    components => $components,
    require => Class['qt::downloader'],
  }
}