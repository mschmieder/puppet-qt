class qt::downloader ( $settings = {} ) inherits ::qt::params {

  validate_hash($settings)
  $_downloader_settings = merge($::qt::downloader_defaults, $settings)
    
  wget::fetch { "download_qt_installer":
        source      => $_downloader_settings['url'],
        destination => $_downloader_settings['root'],
        timeout     => $_downloader_settings['timeout'],
        verbose     => $_downloader_settings['verbose'],
  }
}