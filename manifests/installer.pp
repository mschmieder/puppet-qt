class qt::installer ($install_settings = {},
                     $components = {}) inherits ::qt::params {

  validate_hash($install_settings)
  $_installer_settings = merge($::qt::installer_defaults, $install_settings)

  $cwd = $_installer_settings['root']
  $installer_name = $_installer_settings['name']
  $installer_root = $_installer_settings['root']
  $destination = $_installer_settings['destination']
    
  file { "installer_exec" :
    path => "$installer_root/$installer_name",
    ensure => 'present',
    mode => '0777',
  }

  file { 'qt_install_default':
    path => "$installer_root/qt_install_default",
    ensure  => file,
    content => template('qt/qt_install_default.erb'),
  }

  notice("installing Qt compontnes ${components} to ${destination}")
  
  if ('windows' == $operatingsystem) { 
    exec { 'qt_install' :
      unless    => "ls $destination",
      cwd       => "$cwd",
      command   => "$installer_root/$installer_name --platform windows --script $installer_root/qt_install_default",
      timeout   => 0,
      require   => [File['qt_install_default'],File["installer_exec"]], 
      provider  => powershell,
      logoutput => true,
      loglevel  => debug,
    }
  }
  else {
    exec { 'qt_install' :
      unless  => "ls $destination 2>/dev/null",
      cwd     => "$cwd",
      command => "$installer_root/$installer_name --platform minimal --silent --script $installer_root/qt_install_default",
      path    => ["/usr/bin", "/usr/sbin","/bin/", "/tmp/"],
      timeout => 0,
      require => [File['qt_install_default'],File["installer_exec"]], 
      logoutput => true,
      loglevel => debug,
    }
  }

}