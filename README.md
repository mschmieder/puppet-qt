A Puppet module to download and install qt with the qt online installer

# Requirements
   - puppet-wget
   - puppet-stdlib

# Supported Systems
The following systems are supported. 
   - Debian
   - Ubuntu
   - Windows (work in progress)

# Example
The following example will download and install qt to **/opt/qt/**, installing 

   - Complete Qt5.5
   - GCC [64bit] Version of Qt5.4
   - Android [armv7] Version of Qt5.4
   
```ruby
class { 'qt' : 
   root => '/opt/qt',
   components => {
                   "5.5" => [''],
                   "5.4" => ['gcc_64','android_armv7']
                 }
}
```

# Known Issues
As of this version (0.0.1), it is not 

   + possible to add or remove modules from an already installed version of Qt.
   + possible to use the update mechanism


# License

Copyright Matthias Schmieder

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

