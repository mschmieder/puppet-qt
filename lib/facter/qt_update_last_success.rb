require 'facter'

Facter.add('qt_update_last_success') do
  confine :osfamily => 'Debian'
  setcode do
    if File.exists?('/tmp/update-qt-stamp')
      #get epoch time
      lastsuccess = File.mtime('/tmp/update-qt-stamp').to_i
      lastsuccess
    else
      lastsuccess = -1
      lastsuccess
    end
  end
end
